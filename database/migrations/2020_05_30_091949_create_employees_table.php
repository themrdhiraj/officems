<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('employee_name');
            $table->string('employee_contact');
            $table->string('employee_street_address');
            $table->string('employee_city_address');
            $table->string('employee_state_address');
            $table->string('employee_email');
            $table->date('employee_dob');
            $table->integer('employee_gender');
            $table->integer('employee_department');
            $table->integer('employee_working_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
