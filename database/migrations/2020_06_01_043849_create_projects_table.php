<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('project_code');
            $table->string('project_name');
            $table->integer('project_imp_type');
            $table->string('project_client_name');
            $table->string('project_client_contact');
            $table->string('project_client_email')->nullable($value = true);
            $table->date('project_due_date');
            $table->string('project_handler')->nullable($value = true);
            $table->string('project_value')->nullable($value = true);
            $table->string('project_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
