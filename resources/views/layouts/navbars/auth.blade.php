<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
        <a href="/" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="{{ asset('paper') }}/img/logo-small.png">
            </div>
        </a>
        <a href="/" class="simple-text logo-normal">
            {{ __('TheMrDhiraj') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ $elementActive == 'dashboard' ? 'active' : '' }}">
                <a href="/">
                    <i class="nc-icon nc-bank"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>

            <li class="{{ $elementActive == 'viewProject' || $elementActive == 'addProject' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#project">
                    <i class="nc-icon nc-app"></i>
                    <p>
                            {{ __('Project') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'viewProject' || $elementActive == 'addProject' ? 'show' : '' }}" id="project">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'viewProject' ? 'active' : '' }}">
                            <a href="/project">
                                <span class="sidebar-mini-icon">{{ __('VP') }}</span>
                                <span class="sidebar-normal">{{ __(' View project ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'addProject' ? 'active' : '' }}">
                            <a href="/project/create">
                                <span class="sidebar-mini-icon">{{ __('AP') }}</span>
                                <span class="sidebar-normal">{{ __(' Add project ') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="{{ $elementActive == 'viewEmployee' || $elementActive == 'addEmployee' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#employee">
                    <i class="nc-icon nc-single-02"></i>
                    <p>
                            {{ __('Employee') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'viewEmployee' || $elementActive == 'addEmployee' ? 'show' : '' }}" id="employee">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'viewEmployee' ? 'active' : '' }}">
                            <a href="/employee">
                                <span class="sidebar-mini-icon">{{ __('VE') }}</span>
                                <span class="sidebar-normal">{{ __(' View employees ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'addEmployee' ? 'active' : '' }}">
                            <a href="/employee/create">
                                <span class="sidebar-mini-icon">{{ __('AE') }}</span>
                                <span class="sidebar-normal">{{ __(' Add employee ') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="{{ $elementActive == 'viewNotice' || $elementActive == 'addNotice' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#notice">
                    <i class="nc-icon nc-send"></i>
                    <p>
                            {{ __('Notice') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'viewNotice' || $elementActive == 'addNotice' ? 'show' : '' }}" id="notice">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'viewNotice' ? 'active' : '' }}">
                            <a href="/notice">
                                <span class="sidebar-mini-icon">{{ __('VE') }}</span>
                                <span class="sidebar-normal">{{ __(' View notices ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'addNotice' ? 'active' : '' }}">
                            <a href="/notice/create">
                                <span class="sidebar-mini-icon">{{ __('AE') }}</span>
                                <span class="sidebar-normal">{{ __(' Add notices ') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>


        </ul>
    </div>
</div>