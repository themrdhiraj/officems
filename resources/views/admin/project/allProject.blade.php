@extends('layouts.app', [
'class' => '',
'elementActive' => 'viewProject'
])
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"> Projects list</h4>
            </div>
            <div class="card-body">
                @if(count ($projects) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="text-primary">
                            <th>#</th>
                            <th>Project code</th>
                            <th>Importance of project</th>
                            <th>Project name</th>
                            <th>Project value</th>
                            <th>Handler name</th>
                            <th>Client name</th>
                            <th>Client contact</th>
                            <th>Client e-mail</th>
                            <th>Project due date</th>
                            <th>Status</th>
                        </thead>
                        <tbody>
                            @php $i = 1 @endphp
                            @foreach($projects as $project)
                            <tr>
                                <td>{{$i++}}</td>
                                <td class="text-nowrap">{{$project->project_code}}</td>
                                <td>
                                    @if($project->project_imp_type == 1)
                                        <span class="badge badge-info">Important</span>
                                    @elseif($project->project_imp_type == 2)
                                        <span class="badge badge-success">General</span>
                                    @else
                                        <span class="badge badge-warning">VIP</span>
                                    @endif
                                </td>
                                <td class="text-nowrap">{{$project->project_name}}</td>
                                <td class="text-nowrap">{{$project->project_value}}|-</td>
                                <td class="text-nowrap"><a href="employee/{{$project->project_handler}}">{{$project->employee_name}}</a></td>
                                <td class="text-nowrap">{{$project->project_client_name}}</td>
                                <td class="text-nowrap">{{$project->project_client_contact}}</td>
                                <td class="text-nowrap">{{$project->project_client_email}}</td>
                                <td class="text-nowrap">{{$project->project_due_date}}</td>
                                <td class="text-nowrap">
                                    @if($project->project_status == 0)
                                        <span class="badge badge-danger">Project not started.</span>
                                    @elseif($project->project_status == 1)
                                        <span class="badge badge-primary">Ongoing project.</span>
                                    @else
                                        <span class="badge badge-dark">Rejected !</span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{$projects->links()}}
                @else
                No projects added yet.
                @endif
            </div>
        </div>
    </div>
</div>
@endsection