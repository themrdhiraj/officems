@extends('layouts.app', [
'class' => '',
'elementActive' => 'addProject'
])
@section('content')
{{Form::open(['action' => 'ProjectsController@store', 'method' =>'POST'])}}
<div class="card">
    <div class="card-header">
        <h5 class="title">{{ __('Add project details') }}</h5>
    </div>
    <div class="card-body">
        <div class="row">
            {{Form::label('project_name','Project name',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::text('project_name','',['class' => 'form-control', 'placeholder' => 'School management system'])}}
                </div>
            </div>
        </div>
        <div class="row">
            {{Form::label('project_imp_type','Importance type',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::select('project_imp_type', ['1' => 'Important', '2' => 'General', '3' => 'VIP'], null, ['placeholder' => 'Select a importance type', 'class' => 'form-control'])}}
                </div>
            </div>
        </div>

        <div class="row">
            {{Form::label('project_due_date','Project\'s last due date',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::date('project_due_date','',['class' => 'form-control'])}}
                </div>
            </div>
        </div>

        <div class="row">
            {{Form::label('project_handler','Project handler',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    @if(count ($employees) > 0)
                            <select name="project_handler" class="form-control">
                            <option disabled selected>Select handler</option>
                        @foreach($employees as $employee)
                                <option value="{{$employee->id}}">{{$employee->employee_name}}</option>
                        @endforeach
                            </select>
                    @else
                        You have no employees
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            {{Form::label('project_value','Project Value',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::number('project_value','',['class' => 'form-control', 'placeholder' => '15000'])}}
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            {{Form::label('project_client_name','Client name',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::text('project_client_name','',['class' => 'form-control', 'placeholder' => 'Ram'])}}
                </div>
            </div>
        </div>

        <div class="row">
            {{Form::label('project_client_contact','Client contact no.',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::tel('project_client_contact','',['class' => 'form-control', 'placeholder' => '9800000000', 'pattern' => '[0-9]{10}'])}}
                </div>
            </div>
        </div>

        <div class="row">
            {{Form::label('project_client_email','Client name',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::email('project_client_email','',['class' => 'form-control', 'placeholder' => 'client@example.com'])}}
                </div>
            </div>
        </div>

    </div>
    
    <div class="card-footer ">
        <div class="row">
            <button type="submit" class="btn btn-info btn-round">{{ __('Create project') }}</button>
        </div>
    </div>
</div>
{{Form::close()}}
@endsection