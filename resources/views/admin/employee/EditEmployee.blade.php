@extends('layouts.app', [
'class' => '',
'elementActive' => 'editEmployee'
])
@section('content')
{{Form::open(['action' => ['EmployeesController@update', $employee->id], 'method' =>'POST', 'enctype' => 'multipart/form-data'])}}
<div class="card">
    <div class="card-header">
        <h5 class="title">{{ __('Update employee details') }}</h5>
    </div>
    <div class="card-body">
        <div class="row">
            {{Form::label('employee_name','Employee name',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::text('employee_name',$employee->employee_name,['class' => 'form-control', 'placeholder' => 'Ram Shrestha'])}}
                </div>
            </div>
        </div>
        <div class="row">
            {{Form::label('employee_contact','Employee contact',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::number('employee_contact',$employee->employee_contact,['class' => 'form-control', 'placeholder' => '9800000000'])}}
                </div>
            </div>
        </div>
        <div class="row">
            {{Form::label('employee_street_address','Street Address',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::text('employee_street_address',$employee->employee_street_address,['class' => 'form-control', 'placeholder' => 'Thimi'])}}
                </div>
            </div>
        </div>
        <div class="row">
            {{Form::label('employee_city_address','City Address',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::text('employee_city_address',$employee->employee_city_address,['class' => 'form-control', 'placeholder' => 'Bhaktapur'])}}
                </div>
            </div>
        </div>
        <div class="row">
            {{Form::label('employee_state_address','State',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::select('employee_state_address', ['1' => 'State one', '2' => 'State two', '3' => 'State three', '4' => 'State four', '5' => 'State five', '6' => 'State six', '7' => 'State seven'], $employee->employee_state_address, ['placeholder' => 'Select a state', 'class' => 'form-control'])}}
                </div>
            </div>
        </div>
        <div class="row">
            {{Form::label('employee_email','E-mail',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::email('employee_email',$employee->employee_email,['class' => 'form-control', 'placeholder' => 'someone@email.com'])}}
                </div>
            </div>
        </div>
        <div class="row">
            {{Form::label('employee_dob','Date of birth',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::date('employee_dob',$employee->employee_dob,['class' => 'form-control'])}}
                </div>
            </div>
        </div>
        <div class="row">
            {{Form::label('employee_gender','Gender',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::select('employee_gender', ['1' => 'Male', '2' => 'Female', '3' => 'Other'], $employee->employee_gender, ['placeholder' => 'Select a gender', 'class' => 'form-control'])}}
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            {{Form::label('employee_department','Department',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::select('employee_department', ['1' => 'Accounts and finance', '2' => 'HR', '3' => 'Sales and marketing', '4' => 'Infrastructures', '5' => 'Research and development', '6' => 'Learning and development', '7' => 'IT services', '8' => 'Product development', '9' => 'Admin department', '10' => 'Security and transport', '11' => 'Intern'], $employee->employee_department, ['placeholder' => 'Select a department', 'class' => 'form-control'])}}
                </div>
            </div>
        </div>
        <div class="row">
            {{Form::label('employee_working_time','Working time',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::select('employee_working_time', ['1' => 'Full time', '2' => 'Part time', '3' => 'Casual', '4' => 'Probation'], $employee->employee_working_time, ['placeholder' => 'Select working time', 'class' => 'form-control'])}}
                </div>
            </div>
        </div>
    </div>
    
    <div class="card-footer ">
        <div class="row">
            {{Form::hidden('_method','PUT')}}
            <button type="submit" class="btn btn-info btn-round">{{ __('Update Employee') }}</button>
        </div>
    </div>
</div>
{{Form::close()}}
@endsection