@extends('layouts.app', [
'class' => '',
'elementActive' => 'viewEmployee'
])
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"> Employee lists</h4>
            </div>
            <div class="card-body">
                @if(count ($employees) > 0)
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>contact</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($employees as $employee)
                            <tr>
                                <td>{{$employee->employee_name}}</td>
                                <td>{{$employee->employee_email}}</td>
                                <td>{{$employee->employee_contact}}</td>
                                <td>
                                    {!! Form::open(['action' => ['EmployeesController@destroy', $employee->id ],'method' => 'POST']) !!}
                                    {{Form::hidden('_method', 'DELETE')}}
                                    
                                    <a href="/employee/{{ $employee->id }}" class="btn btn-primary">View profile</a>
                                    {{Form::submit('Delete',['class' => 'btn btn-dark','onclick' => 'return confirm("Are you sure want to delete?")'])}}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{$employees->links()}}
                @else
                No Employees added yet.
                @endif
            </div>
        </div>
    </div>
</div>
@endsection