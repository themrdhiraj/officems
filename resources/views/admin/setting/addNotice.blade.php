@extends('layouts.app', [
'class' => '',
'elementActive' => 'addNotice'
])
@section('content')
{{Form::open(['action' => 'NoticesController@store', 'method' =>'POST'])}}
<div class="card">
    <div class="card-header">
        <h5 class="title">{{ __('Notice') }}</h5>
    </div>
    <div class="card-body">

        <div class="row">
            {{Form::label('notice_category','Category',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::select('notice_category', ['1' => 'Office', '2' => 'Staff', '3' => 'Project', '4' => 'Others'], null, ['placeholder' => 'Select a category', 'class' => 'form-control'])}}
                </div>
            </div>
        </div>
        
        <div class="row">
            {{Form::label('notice_title','Title',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::text('notice_title','',['class' => 'form-control', 'placeholder' => 'Meeting'])}}
                </div>
            </div>
        </div>

        <div class="row">
            {{Form::label('notice_contents','Content',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::textarea('notice_contents','',['class' => 'form-control','rows' => '1', 'placeholder' => 'Meeting contents here...'])}}
                </div>
            </div>
        </div>
    </div>
    
    <div class="card-footer ">
        <div class="row">
            <button type="submit" class="btn btn-info btn-round">{{ __('Submit notice') }}</button>
        </div>
    </div>
</div>
{{Form::close()}}
@endsection('content')