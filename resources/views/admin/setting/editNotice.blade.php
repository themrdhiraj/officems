@extends('layouts.app', [
'class' => '',
'elementActive' => 'addNotice'
])
@section('content')
{{Form::open(['action' => ['NoticesController@update', $notice->id], 'method' =>'POST'])}}
<div class="card">
    <div class="card-header">
        <h5 class="title">{{ __('Edit notice') }}</h5>
    </div>
    <div class="card-body">

        <div class="row">
            {{Form::label('notice_category','Category',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::select('notice_category', ['1' => 'Office', '2' => 'Staff', '3' => 'Project', '4' => 'Others'], $notice->notice_category, ['placeholder' => 'Select a category', 'class' => 'form-control'])}}
                </div>
            </div>
        </div>
        
        <div class="row">
            {{Form::label('notice_title','Title',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::text('notice_title',$notice->notice_title,['class' => 'form-control', 'placeholder' => 'Meeting'])}}
                </div>
            </div>
        </div>

        <div class="row">
            {{Form::label('notice_contents','Content',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::textarea('notice_contents',$notice->notice_contents,['class' => 'form-control','rows' => '1', 'placeholder' => 'Meeting contents here...'])}}
                </div>
            </div>
        </div>
        <div class="row">
            {{Form::label('notice_status','Status',['class' => 'col-md-2 col-form-label'])}}
            <div class="col-md-10">
                <div class="form-group">
                    {{Form::select('notice_status', ['1' => 'Active', '2' => 'Inactive'], $notice->notice_status, ['placeholder' => 'Change a status', 'class' => 'form-control'])}}
                </div>
            </div>
        </div>
    </div>
    
    <div class="card-footer ">
        <div class="row">
            {{Form::hidden('_method', 'PUT')}}
            <button type="submit" class="btn btn-info btn-round">{{ __('Update notice') }}</button>
        </div>
    </div>
</div>
{{Form::close()}}
@endsection('content')