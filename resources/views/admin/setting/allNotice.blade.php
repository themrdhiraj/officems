@extends('layouts.app', [
'class' => '',
'elementActive' => 'viewNotice'
])
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"> Notices list</h4>
            </div>
            <div class="card-body">
                @if(count ($notices) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="text-primary">
                            <th>#</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Contents</th>
                            <th>Status</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @php $i = 1 @endphp
                            @foreach($notices as $notice)
                            <tr>
                                <td>{{$i++}}</td>
                                <td class="text-nowrap">{{$notice->notice_title}}</td>
                                <td>
                                    @if($notice->notice_category == 1)
                                        Office
                                    @elseif($notice->notice_category == 2)
                                        Staff
                                    @elseif($notice->notice_category == 3)
                                        Project
                                    @else
                                        Others
                                    @endif
                                </td>
                                <td>{{$notice->notice_contents}}</td>
                                <td>
                                    @if($notice->notice_status == 2)
                                        <span class="badge badge-dark">Inactive</span>
                                    @elseif($notice->notice_status == 1)
                                        <span class="badge badge-primary">Active</span>
                                    @else
                                        <span class="badge badge-danger">Unknown error</span>
                                    @endif
                                </td>
                                <td>
                                    {!! Form::open(['action' => ['NoticesController@destroy', $notice->id ],'method' => 'POST']) !!}
                                    {{Form::hidden('_method', 'DELETE')}}
                                    
                                    <a href="/notice/{{ $notice->id }}/edit" class="btn btn-primary">Edit</a>
                                    {{Form::submit('Delete',['class' => 'btn btn-dark','onclick' => 'return confirm("Are you sure want to delete?")'])}}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{$notices->links()}}
                @else
                No notices added yet.
                @endif
            </div>
        </div>
    </div>
</div>
@endsection