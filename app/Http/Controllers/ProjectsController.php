<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Employee;

class ProjectsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::orderBy('projects.id','desc')->join('employees', 'employees.id','=','projects.project_handler')->paginate(5);
        return view('admin.project.allProject')->with('projects', $projects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::all();
        return view('admin.project.addProject')->with('employees', $employees);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'project_name' => 'required',
            'project_imp_type' => 'required',
            'project_due_date' => 'required',
            'project_client_name' => 'required',
            'project_client_contact' => 'required',
            'project_client_email' => 'required',
            'project_handler' => 'required',
        ]);

        $project = new Project;
        $project->project_code = 'PRJ-'.date('ymd').'-'.rand(10,99).$request->input('project_name')[0];
        $project->project_name = $request->input('project_name');
        $project->project_imp_type = $request->input('project_imp_type');
        $project->project_due_date = $request->input('project_due_date');
        $project->project_client_email = $request->input('project_client_email');
        $project->project_client_name = $request->input('project_client_name');
        $project->project_client_contact = $request->input('project_client_contact');
        $project->project_handler = $request->input('project_handler');
        $project->project_value = $request->input('project_value');

        $success = $project->save();
        if ($success) {
            return redirect('/project')->with('success','Project added successfully.');
        }else{
            return back('error', 'There was a problem while adding project!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
