<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;

class EmployeesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array(
            'employees' => Employee::orderBy('id','desc')->paginate(5)
        );
        return view('admin.employee.ViewEmployee')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.employee.AddEmployee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'employee_name' => 'required',
            'employee_contact' => 'required',
            'employee_email' => 'required',
            'employee_street_address' => 'required',
            'employee_city_address' => 'required',
            'employee_state_address' => 'required',
            'employee_dob' => 'required',
            'employee_gender' => 'required',
            'employee_department' => 'required',
            'employee_working_time' => 'required',
        ]);

        $employee = new Employee;
        $employee->employee_name = $request->input('employee_name');
        $employee->employee_contact = $request->input('employee_contact');
        $employee->employee_email = $request->input('employee_email');
        $employee->employee_street_address = $request->input('employee_street_address');
        $employee->employee_city_address = $request->input('employee_city_address');
        $employee->employee_state_address = $request->input('employee_state_address');
        $employee->employee_dob = $request->input('employee_dob');
        $employee->employee_gender = $request->input('employee_gender');
        $employee->employee_department = $request->input('employee_department');
        $employee->employee_working_time = $request->input('employee_working_time');
        $employee->user_id = auth()->user()->id;

        $success = $employee->save();

        if ($success) {
            return redirect('/employee')->with('success','Employee added.');
        }else{
            return redirect('/employee/create')->with('error','There was a problem while adding employee.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);
        return view('admin.employee.profileEmployee')->with('employee', $employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $employee = Employee::find($id);
        return view('employee/EditEmployee')->with('employee', $employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request,[
            'employee_name' => 'required',
            'employee_contact' => 'required',
            'employee_email' => 'required',
            'employee_street_address' => 'required',
            'employee_city_address' => 'required',
            'employee_state_address' => 'required',
            'employee_dob' => 'required',
            'employee_gender' => 'required',
            'employee_department' => 'required',
            'employee_working_time' => 'required',
        ]);

        $employee = Employee::find($id);
        $employee->employee_name = $request->input('employee_name');
        $employee->employee_contact = $request->input('employee_contact');
        $employee->employee_email = $request->input('employee_email');
        $employee->employee_street_address = $request->input('employee_street_address');
        $employee->employee_city_address = $request->input('employee_city_address');
        $employee->employee_state_address = $request->input('employee_state_address');
        $employee->employee_dob = $request->input('employee_dob');
        $employee->employee_gender = $request->input('employee_gender');
        $employee->employee_department = $request->input('employee_department');
        $employee->employee_working_time = $request->input('employee_working_time');
        $employee->user_id = auth()->user()->id;

        $success = $employee->save();

        if ($success) {
            return back()->with('success','Employee updated.');
        }else{
            return back()->with('error','There was a problem while updating employee.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $employee = Employee::find($id);

        $success = $employee->delete();

        if ($success) {
            return redirect('/employee')->with('success', 'Employee '.$employee->employee_name.' Deleted!!!');
        }else{
            return redirect('/employee')->with('error', 'Employee not Deleted!!!');
        }
    }
}
