<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notice;

class NoticesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices = Notice::orderBy('id','desc')->paginate(5);
        return view('admin.setting.allNotice')->with('notices', $notices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.setting.addNotice');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'notice_title' => 'required',
            'notice_category' => 'required',
            'notice_contents' => 'required'
       ]);

       $notice = new Notice;

       $notice->notice_title = $request->input('notice_title');
       $notice->notice_category = $request->input('notice_category');
       $notice->notice_contents = $request->input('notice_contents');
       $notice->user_id = auth()->user()->id;

       $success = $notice->save();

       if ($success) {
           return redirect('notice')->with('success', 'Notice added.');
       }else{
            return back()->withInput()->with('error', 'There was a problem while adding a notice.');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notice = Notice::find($id);
        return view('admin.setting.editNotice')->with('notice', $notice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'notice_title' => 'required',
            'notice_category' => 'required',
            'notice_contents' => 'required',
            'notice_status' => 'required'
       ]);

       $notice = Notice::find($id);

       $notice->notice_title = $request->input('notice_title');
       $notice->notice_category = $request->input('notice_category');
       $notice->notice_contents = $request->input('notice_contents');
       $notice->notice_status = $request->input('notice_status');
       $notice->user_id = auth()->user()->id;

       $success = $notice->save();

       if ($success) {
           return redirect('notice')->with('success', 'Notice updated.');
       }else{
            return back()->withInput()->with('error', 'There was a problem while updating a notice.');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $notice = Notice::find($id);
       $success = $notice->delete();

       if ($success) {
            return redirect('notice')->with('success', 'Notice '.$notice->notice_title.' Deleted!!!');
        }else{
            return back()->with('error', 'Notice not deleted!!!');
        }
    }
}
